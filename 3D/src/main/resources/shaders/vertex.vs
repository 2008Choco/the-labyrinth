#version 330

in vec3 position;
in vec2 inTextureCoords;

out vec2 resultTextureCoords;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

void main(){
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
	resultTextureCoords = inTextureCoords;
}
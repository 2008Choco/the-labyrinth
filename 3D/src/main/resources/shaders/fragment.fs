#version 330

in vec2 resultTextureCoords;

out vec4 fragColor;

uniform sampler2D textureSampler;
uniform vec3 colour;
uniform bool useColour;

void main(){
    if (useColour){
        fragColor = vec4(colour, 1);
    }
    else{
        fragColor = texture(textureSampler, resultTextureCoords);
    }
}
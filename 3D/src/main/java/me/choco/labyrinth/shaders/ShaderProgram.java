package me.choco.labyrinth.shaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

public class ShaderProgram {
	
	private static final String SHADERS_FOLDER = "/shaders/";
	
	private final Map<String, Integer> uniformValues = new HashMap<>();
	
	private final int programID;
	private int vertexShaderID, fragmentShaderID;
	public ShaderProgram(String vertexShaderFile, String fragmentShaderFile) {
		this.programID = GL20.glCreateProgram();
		if (this.programID == 0)
			throw new UnsupportedOperationException("Failed to create a shader program");
		
		this.createVertexShader(vertexShaderFile);
		this.createFragmentShader(fragmentShaderFile);
		this.link();
	}
	
	public void createVertexShader(String fileName) {
		this.vertexShaderID = this.createShader(fileName, GL20.GL_VERTEX_SHADER);
	}
	
	public void createFragmentShader(String fileName) {
		this.fragmentShaderID = this.createShader(fileName, GL20.GL_FRAGMENT_SHADER);
	}
	
	private final int createShader(String fileName, int shaderType) {
		int shaderID = GL20.glCreateShader(shaderType);
		if (shaderID == 0)
			throw new UnsupportedOperationException("Could not create shader");
		
		StringBuilder shaderCode = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(SHADERS_FOLDER + fileName)))){
			String line;
			while ((line = reader.readLine()) != null)
				shaderCode.append(line + "\n");
		}catch(IOException e){ e.printStackTrace(); }
		
		GL20.glShaderSource(shaderID, shaderCode.toString());
		GL20.glCompileShader(shaderID);
		
		if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == 0)
			throw new IllegalStateException("Error while attempting to compile shader (id: " + shaderID + ") \n" + GL20.glGetShaderInfoLog(shaderID));
		
		GL20.glAttachShader(this.programID, shaderID);
		return shaderID;
	}
	
	public void link() {
		GL20.glLinkProgram(this.programID);
		if (GL20.glGetProgrami(this.programID, GL20.GL_LINK_STATUS) == 0)
			System.err.println("There was an issue linking the shader program");
		
		// Should be removed after production is completed. Just for debug
		GL20.glValidateProgram(this.programID);
		if (GL20.glGetProgrami(this.programID, GL20.GL_VALIDATE_STATUS) == 0)
			System.err.println("There was an issue while validating the shader program");
	}
	
	public void bind() {
		GL20.glUseProgram(this.programID);
	}
	
	public void unbind() {
		GL20.glUseProgram(0);
	}
	
	public boolean createUniformValue(String uniformName) {
		int location = GL20.glGetUniformLocation(this.programID, uniformName);
		if (location < 0) return false;
		
		this.uniformValues.put(uniformName, location);
		return true;
	}
	
	public boolean createUniformValues(String... uniformNames) {
		for (String uniformName : uniformNames) 
			if (!this.createUniformValue(uniformName)) return false;
		return true;
	}
	
	public void setUniformValue(String uniformName, Matrix4f value) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
		value.get(buffer);
		
		GL20.glUniformMatrix4fv(this.uniformValues.get(uniformName), false, buffer);
	}
	
	public void setUniformValue(String uniformName, Vector3f value) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
		value.get(buffer);
		
		GL20.glUniformMatrix4fv(this.uniformValues.get(uniformName), false, buffer);
	}
	
	public void setUniformValue(String uniformName, int value) {
		GL20.glUniform1i(this.uniformValues.get(uniformName), value);
	}
	
	public void setUniformValue(String uniformName, boolean value) {
		this.setUniformValue(uniformName, value ? 1 : 0);
	}
	
	public void cleanup() {
		this.unbind();
		if (this.programID != 0){
			if (this.vertexShaderID != 0) GL20.glDetachShader(this.programID, this.vertexShaderID);
			if (this.fragmentShaderID != 0) GL20.glDetachShader(this.programID, this.fragmentShaderID);
			GL20.glDeleteProgram(this.programID);
		}
	}
}
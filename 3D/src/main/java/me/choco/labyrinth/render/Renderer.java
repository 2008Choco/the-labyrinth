package me.choco.labyrinth.render;

import java.util.List;

import org.joml.Matrix4f;

import me.choco.labyrinth.Labyrinth;
import me.choco.labyrinth.entity.Entity;
import me.choco.labyrinth.models.Model;
import me.choco.labyrinth.shaders.ShaderProgram;
import me.choco.labyrinth.utils.Camera;
import me.choco.labyrinth.utils.MatrixMath;

public class Renderer {

	private static final float FOV = (float) Math.toRadians(60.0);
	private static final float Z_NEAR = 0.01f, Z_FAR = 1000f;
	
	public void render(ShaderProgram shader, Camera camera, List<Entity> entities) {
		shader.bind();
		
		// Projection matrix
		Matrix4f projectionMatrix = MatrixMath.getProjectionMatrix(FOV, Labyrinth.width, Labyrinth.height, Z_NEAR, Z_FAR);
		shader.setUniformValue("projectionMatrix", projectionMatrix);
		
		// Camera matrix
		Matrix4f cameraViewMatrix = MatrixMath.getViewMatrix(camera);
		
		shader.setUniformValue("textureSampler", 0);
		
		for (Entity entity : entities){
			Model model = entity.getModel();
			
			Matrix4f worldMatrix = MatrixMath.getModelViewMatrix(entity, cameraViewMatrix);
			shader.setUniformValue("modelViewMatrix", worldMatrix);
			
			shader.setUniformValue("colour", model.getColour());
			shader.setUniformValue("useColour", model.hasTexture() ? false : true);
			
			entity.render();
		}
		
		shader.unbind();
	}
}
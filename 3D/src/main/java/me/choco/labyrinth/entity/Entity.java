package me.choco.labyrinth.entity;

import org.joml.Vector3f;

import me.choco.labyrinth.models.Model;

public class Entity {
	
	private Vector3f location, rotation;
	private float scale;
	
	private final Model model;
	public Entity(Model model) {
		this.model = model;
		this.location = new Vector3f(0, 0, 0);
		this.rotation = new Vector3f(0, 0, 0);
		this.scale = 1;
	}
	
	public Model getModel() {
		return model;
	}
	
	public void setLocation(float x, float y, float z) {
		this.location.x = x;
		this.location.y = y;
		this.location.z = z;
	}
	
	public Vector3f getLocation() {
		return location;
	}
	
	public void setRotation(float x, float y, float z) {
		this.rotation.x = x;
		this.rotation.y = y;
		this.rotation.z = z;
	}
	
	public void rotate(float x, float y, float z) {
		this.rotation.x += x;
		this.rotation.y += y;
		this.rotation.z += z;
	}
	
	public Vector3f getRotation() {
		return rotation;
	}
	
	public void setScale(float scale) {
		this.scale = scale;
	}
	
	public float getScale() {
		return scale;
	}
	
	public void update(){}
	
	public void render(){
		this.model.render();
	}
}
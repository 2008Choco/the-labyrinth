package me.choco.labyrinth.models;

import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Texture {
	
	// TEXTURES
	public static Texture GRASS_BLOCK;
	public static Texture WHITE;
	
	
	private static final String TEXTURES_FOLDER = "/textures/";
	
	private final int textureID;
	public Texture(String fileName) {
		int id;
		try{
			PNGDecoder image = new PNGDecoder(Texture.class.getResourceAsStream(TEXTURES_FOLDER + fileName));
			
			ByteBuffer buffer = ByteBuffer.allocateDirect(4 * image.getWidth() * image.getHeight());
			image.decode(buffer, image.getWidth() * 4, Format.RGBA);
			buffer.flip();
			
			id = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, id);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
			glGenerateMipmap(GL_TEXTURE_2D);
		}catch(IOException e) {
			System.err.println("Could not load texture " + fileName);
			e.printStackTrace();
			id = -1;
		}
		
		this.textureID = id;
	}
	
	public int getTextureID() {
		return textureID;
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	public void delete() {
		GL11.glDeleteTextures(textureID);
	}
	
	public static void loadTextures() {
		GRASS_BLOCK = new Texture("cube_texture.png");
		WHITE = new Texture("white.png");
	}
}
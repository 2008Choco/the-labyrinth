package me.choco.labyrinth.models;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

public class Model {
	
	private static final Vector3f DEFAULT_COLOUR = new Vector3f(1.0f, 1.0f, 1.0f);
	
	private Vector3f colour;
	private Texture texture;
	
	private final int[] vbos;
	private final int vaoID, vertexCount;
	
	public Model(float[] vertices, int[] indices, float[] textureCoords, float[] normals, Texture texture) {
		this.vertexCount = indices.length;
		this.texture = texture;
		this.colour = DEFAULT_COLOUR;
		
		// Creating a VAO
		this.vaoID = glGenVertexArrays();
		glBindVertexArray(this.vaoID);
		
		// Vertices VBO
		FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(vertices.length);
		verticesBuffer.put(vertices).flip();
		
		int verticesVboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, verticesVboID);
		glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
		
		// Indices VBO
		IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indices.length);
		indicesBuffer.put(indices).flip();
		
		int indicesVboID = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);
		
		// Textures VBO
		FloatBuffer textureCoordsBuffer = BufferUtils.createFloatBuffer(textureCoords.length);
		textureCoordsBuffer.put(textureCoords).flip();
		
		int textureCoordsVboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, textureCoordsVboID);
		glBufferData(GL_ARRAY_BUFFER, textureCoordsBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
		
		// Normals VBO
		FloatBuffer normalsBuffer = BufferUtils.createFloatBuffer(normals.length);
		normalsBuffer.get(normals).flip();
		
		int normalsVboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, normalsVboID);
		glBufferData(GL_ARRAY_BUFFER, normalsBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
		
		// Unbinding
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		
		this.vbos = new int[]{verticesVboID, indicesVboID, textureCoordsVboID, normalsVboID};
	}
	
	public Model(float[] vertices, int[] indices, float[] textureCoords, float[] normals) {
		this(vertices, indices, textureCoords, normals, null);
	}
	
	public int getVertexCount() {
		return vertexCount;
	}
	
	public int getVaoID() {
		return vaoID;
	}
	
	public int getVerticesVboID() {
		return this.vbos[0];
	}
	
	public int getIndicesBboID() {
		return this.vbos[1];
	}
	
	public int getTextureCoordsVboID() {
		return this.vbos[2];
	}
	
	public int getNormalsVboID() {
		return this.vbos[3];
	}
	
	public void setColour(Vector3f colour) {
		this.colour = colour;
	}
	
	public Vector3f getColour() {
		return colour;
	}
	
	public void setTexture(Texture texture) {
		this.texture = texture;
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public boolean hasTexture() {
		return this.texture != null;
	}
	
	public void render() {
		// Bind it
		glBindVertexArray(this.vaoID);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		if (this.texture != null){
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			this.texture.bind();
		}
		
		// Draw it
		glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
		
		// Unbind it
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public void cleanup() {
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteBuffers(this.getVerticesVboID());
		glDeleteBuffers(this.getTextureCoordsVboID());
		glDeleteBuffers(this.getIndicesBboID());
		
		if (this.texture != null) this.texture.delete();
		
		glBindVertexArray(0);
		glDeleteVertexArrays(this.vaoID);
	}
}
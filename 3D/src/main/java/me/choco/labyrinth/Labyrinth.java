package me.choco.labyrinth;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

import me.choco.labyrinth.entity.Entity;
import me.choco.labyrinth.models.Texture;
import me.choco.labyrinth.render.Renderer;
import me.choco.labyrinth.shaders.ShaderProgram;
import me.choco.labyrinth.utils.Camera;
import me.choco.labyrinth.utils.EntityHandler;
import me.choco.labyrinth.utils.ModelLoader;
import me.choco.labyrinth.utils.input.KeyboardInput;
import me.choco.labyrinth.utils.input.MouseInput;

public class Labyrinth {
	
	/* 
	 * LWJGL 3 Informational Book (GitBook)
	 * https://lwjglgamedev.gitbooks.io/3d-game-development-with-lwjgl/content/
	 */
	
	private static final int MAX_FPS = 100, MAX_UPS = 20;
	private static final float MOUSE_SENSITIVITY = 0.25f, CAMERA_SPEED = 0.00000075f;
	private static final boolean VSYNC_ENABLED = false;
	private static final boolean WIREFRAME_MODE = true;
	
	private int currentFPS = 0, currentUPS = 0;
	public static int width, height;
	private boolean running = true;
	
	private MouseInput mouseInput;
	private KeyboardInput keyInput;
	
	private ShaderProgram shader;
	private Camera camera;
	private Renderer renderer;
	private EntityHandler entityHandler;
	
	private Entity entity;

	private long window;
	
	public Labyrinth() {
		System.out.println("The Labyrinth");

		try {
			this.run();
		} finally {
			glfwFreeCallbacks(window);
			glfwDestroyWindow(window);
			
			glfwTerminate();
			glfwSetErrorCallback(null).free();
		}
	}

	private void init() {
		GLFWErrorCallback.createPrint(System.err).set();

		if (!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		
		width = vidmode.width() - 600;
		height = vidmode.height() - 250;

		window = glfwCreateWindow(width, height, "The Labyrinth", NULL, NULL);
		if (window == NULL)
			throw new UnsupportedOperationException("Failed to create the GLFW window");
		
		glfwMakeContextCurrent(window);
		GL.createCapabilities();

		// Callbacks
		glfwSetWindowSizeCallback(window, (window, width, height) -> {
			GL11.glViewport(0, 0, Labyrinth.width = width, Labyrinth.height = height);
		});

		glfwSetWindowPos(window, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);
		GLFW.glfwSetCursorPos(window, width / 2, height / 2);
		
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		glfwSwapInterval(VSYNC_ENABLED ? 1 : 0);
		glfwShowWindow(window);
		if (WIREFRAME_MODE) GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		
		Texture.loadTextures();
		
		// Game Field Initialization
		this.mouseInput = new MouseInput(window);
		this.keyInput = new KeyboardInput(window);
		
		this.shader = new ShaderProgram("vertex.vs", "fragment.fs");
		this.shader.createUniformValues("projectionMatrix", "modelViewMatrix", "textureSampler", "colour", "useColour");
		
		this.camera = new Camera();
		this.renderer = new Renderer();
		this.entityHandler = new EntityHandler();
		
		this.entity = new Entity(ModelLoader.loadOBJModel("Porsche_911_GT2.obj"));
		this.entity.setLocation(0, 0, -5);
		this.entityHandler.addEntity(entity);
	}

	public void update(){
		this.entity.rotate(0, 2, 0);
	}
	
	public void render(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0, 0, 0, 0.0f);

		this.renderer.render(shader, camera, this.entityHandler.getEntities());
		
		glfwSwapBuffers(window);

		glfwPollEvents();
	}
	
	public void input() {
		// Keyboard input
		this.camera.resetNextMove();
		if (this.keyInput.isKeyPressed(GLFW_KEY_W)) this.camera.incrementNextMove(0, 0, -1);
		if (this.keyInput.isKeyPressed(GLFW_KEY_S)) this.camera.incrementNextMove(0, 0, 1);
		if (this.keyInput.isKeyPressed(GLFW_KEY_A)) this.camera.incrementNextMove(-1, 0, 0);
		if (this.keyInput.isKeyPressed(GLFW_KEY_D)) this.camera.incrementNextMove(1, 0, 0);
		if (this.keyInput.isKeyPressed(GLFW_KEY_LEFT_SHIFT)) this.camera.incrementNextMove(0, -1, 0);
		if (this.keyInput.isKeyPressed(GLFW_KEY_SPACE)) this.camera.incrementNextMove(0, 1, 0);

		// Mouse input
		this.mouseInput.handleInput();
		Vector3f nextMove = this.camera.getNextMove();
		camera.relocate(nextMove.x * CAMERA_SPEED, nextMove.y * CAMERA_SPEED, nextMove.z * CAMERA_SPEED); 
		
		if (this.mouseInput.isRightButtonPressed()){
			Vector2f rotVec = this.mouseInput.getDisplayVector();
			camera.rotate(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
		}
	}
	
	public void run() {
		this.init();
		
		long startTime = System.currentTimeMillis();
		long lastTimeUPS = System.nanoTime(), lastTimeFPS = System.nanoTime();
		double deltaFPS = 0.0, deltaUPS = 0.0;
		double nsFPS = 1_000_000_000.0 / MAX_FPS, nsUPS = 1_000_000_000.0 / MAX_UPS;
		
		while (running) {
			// Game updates
			long now = System.nanoTime();
			deltaUPS += (now - lastTimeUPS) / nsUPS;
			lastTimeUPS = now;

			if (deltaUPS >= 1.0) {
				this.update();
				this.currentUPS++;
				deltaUPS--;
			}
			
			// Frame render updates
			now = System.nanoTime();
			deltaFPS += (now - lastTimeFPS) / nsFPS;
			lastTimeFPS = now;
			
			if (deltaFPS >= 1.0) {
				this.render();
				this.currentFPS++;
				deltaFPS--;
			}

			// FPS and UPS display
			if (System.currentTimeMillis() - startTime > 1000) {
				startTime += 1000;
				GLFW.glfwSetWindowTitle(window, "The Labyrinth - (FPS: " + this.currentFPS + " | UPS: " + this.currentUPS + ")");
				this.currentUPS = 0;
				this.currentFPS = 0;
			}
			
			this.input();

			if (glfwWindowShouldClose(window)) running = false;
		}

		this.cleanup();
	}
	
	public Camera getCamera() {
		return camera;
	}
	
	public EntityHandler getEntityHandler() {
		return entityHandler;
	}
	
	public void cleanup(){
		if (this.shader != null) this.shader.cleanup();
		this.entityHandler.cleanup();
	}
	
	public static void main(String[] args) { new Labyrinth(); }
}
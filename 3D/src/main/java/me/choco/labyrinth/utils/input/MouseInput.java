package me.choco.labyrinth.utils.input;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_1;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_2;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwSetCursorEnterCallback;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

import org.joml.Vector2d;
import org.joml.Vector2f;

public class MouseInput {

    private final Vector2f displayVector;
    private final Vector2d previousPosition, currentPosition;
    private boolean inWindow = false, leftButtonPressed = false, rightButtonPressed = false;

    public MouseInput(long window) {
        previousPosition = new Vector2d(-1, -1);
        currentPosition = new Vector2d(0, 0);
        displayVector = new Vector2f();
        
        glfwSetCursorPosCallback(window, (context, xpos, ypos) -> {
        	this.currentPosition.x = xpos;
        	this.currentPosition.y = ypos;
        });
        
        glfwSetMouseButtonCallback(window, (context, button, action, mods) -> {
            this.leftButtonPressed = (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS);
            this.rightButtonPressed = (button == GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS);
        });
        
        glfwSetCursorEnterCallback(window, (context, entered) -> this.inWindow = entered);
    }

    public Vector2f getDisplayVector() {
        return displayVector;
    }

    public void handleInput() {
    	if (!inWindow) return;
        
    	displayVector.x = 0;
        displayVector.y = 0;
        
        if (previousPosition.x > 0 && previousPosition.y > 0) {
            double deltaX = currentPosition.x - previousPosition.x;
            double deltaY = currentPosition.y - previousPosition.y;
            boolean rotateX = (deltaX != 0);
            boolean rotateY = (deltaY != 0);
            
            if (rotateX) displayVector.y = (float) deltaX;
            if (rotateY) displayVector.x = (float) deltaY;
        }
        
        previousPosition.x = currentPosition.x;
        previousPosition.y = currentPosition.y;
    }

    public boolean isLeftButtonPressed() {
        return leftButtonPressed;
    }

    public boolean isRightButtonPressed() {
        return rightButtonPressed;
    }
}
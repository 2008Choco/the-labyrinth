package me.choco.labyrinth.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

import me.choco.labyrinth.models.Model;
import me.choco.labyrinth.models.Texture;

public class ModelLoader {
	
	private static final String MODELS_FOLDER = "/models/";
	
	public static Model loadOBJModel(String fileName, Texture texture){
		BufferedReader reader = new BufferedReader(new InputStreamReader(ModelLoader.class.getResourceAsStream(MODELS_FOLDER + fileName)));
		
		List<Vector3f> vertices = new ArrayList<>();
		List<Vector2f> textureCoords = new ArrayList<>();
		List<Vector3f> normals = new ArrayList<>();
		List<Integer> indices = new ArrayList<>();
		
		float[] verticesArray = null, textureCoordsArray = null, normalsArray = null;
		int[] indicesArray = null;
		
		try{
			String line;
			while((line = reader.readLine()) != null){
				String[] currentLine = line.split("\\s+");
				
				switch(currentLine[0]){
				case "v":
					Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
					break;
				case "vt":
					Vector2f textureCoord = new Vector2f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]));
					textureCoords.add(textureCoord);
					break;
				case "vn":
					Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
					normals.add(normal);
					break;
				case "f":
					if (textureCoordsArray == null) textureCoordsArray = new float[vertices.size() * 2];
					if (normalsArray == null) normalsArray = new float[vertices.size() * 3];
					
					String[] vertex1 = currentLine[1].split("/");
					String[] vertex2 = currentLine[2].split("/");
					String[] vertex3 = currentLine[3].split("/");
					
					processVertex(vertex1, indices, textureCoords, normals, textureCoordsArray, normalsArray);
					processVertex(vertex2, indices, textureCoords, normals, textureCoordsArray, normalsArray);
					processVertex(vertex3, indices, textureCoords, normals, textureCoordsArray, normalsArray);
					break;
				default: break;
				}
			}
		}catch(IOException e){ e.printStackTrace(); }
		
		verticesArray = new float[vertices.size() * 3];
		indicesArray = new int[indices.size()];
		
		int vertexPointer = 0;
		for (Vector3f vertex : vertices){
			verticesArray[vertexPointer++] = vertex.x;
			verticesArray[vertexPointer++] = vertex.y;
			verticesArray[vertexPointer++] = vertex.z;
		}
		
		for (int i = 0; i < indices.size(); i++)
			indicesArray[i] = indices.get(i);
		
		return new Model(verticesArray, indicesArray, textureCoordsArray, normalsArray, texture == null ? Texture.WHITE : texture);
	}
	
	public static Model loadOBJModel(String fileName) {
		return loadOBJModel(fileName, null);
	}
	
	private static void processVertex(String[] vertexData, List<Integer> indices, List<Vector2f> textures, List<Vector3f> normals, float[] textureArray, float[] normalsArray){
		int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
		indices.add(currentVertexPointer);
		
		if (vertexData[1].equalsIgnoreCase("")){
			textureArray[currentVertexPointer * 2] = -1;
			textureArray[currentVertexPointer * 2 + 1] = -1;
		}else{
			Vector2f currentTexture = textures.get(Integer.parseInt(vertexData[1]) - 1);
			textureArray[currentVertexPointer * 2] = currentTexture.x;
			textureArray[currentVertexPointer * 2 + 1] = 1 - currentTexture.y;
		}
		
		Vector3f currentNormal = normals.get(Integer.parseInt(vertexData[2]) - 1);
		normalsArray[currentVertexPointer * 3] = currentNormal.x;
		normalsArray[currentVertexPointer * 3 + 1] = currentNormal.y;
		normalsArray[currentVertexPointer * 3 + 2] = currentNormal.z;
	}
}
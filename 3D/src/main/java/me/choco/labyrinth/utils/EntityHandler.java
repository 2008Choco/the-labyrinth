package me.choco.labyrinth.utils;

import java.util.ArrayList;
import java.util.List;

import me.choco.labyrinth.entity.Entity;

public class EntityHandler {
	
	private final List<Entity> entities = new ArrayList<>();
	
	public void addEntity(Entity entity) {
		this.entities.add(entity);
	}
	
	public void removeEntity(Entity entity) {
		this.entities.remove(entity);
	}
	
	public List<Entity> getEntities() {
		return entities;
	}
	
	public void cleanup() {
		for (Entity entity : entities)
			entity.getModel().cleanup();
		this.entities.clear();
	}
}
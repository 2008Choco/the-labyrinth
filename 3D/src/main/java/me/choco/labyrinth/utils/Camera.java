package me.choco.labyrinth.utils;

import org.joml.Vector3f;

public class Camera {
	
	private final Vector3f location, rotation, nextMove;
	public Camera(Vector3f location, Vector3f rotation) {
		this.location = location;
		this.rotation = rotation;
		this.nextMove = new Vector3f();
	}
	
	public Camera() {
		this(new Vector3f(0, 0, 0), new Vector3f(0, 0, 0));
	}
	
	public void setLocation(float x, float y, float z) {
		this.location.x = x;
		this.location.y = y;
		this.location.z = z;
	}
	
    public void relocate(float offsetX, float offsetY, float offsetZ) {
        if (offsetZ != 0){
            location.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
            location.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
        }
        if (offsetX != 0){
            location.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0f * offsetX;
            location.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
        }
        location.y += offsetY;
    }
	
	public Vector3f getLocation() {
		return location;
	}
	
	public void setRotation(float x, float y, float z) {
		this.rotation.x = x;
		this.rotation.y = y;
		this.rotation.z = z;
	}
	
	public void rotate(float x, float y, float z) {
		this.rotation.x += x;
		this.rotation.y += y;
		this.rotation.z += z;
	}
	
	public Vector3f getRotation() {
		return rotation;
	}
	
	public void incrementNextMove(float x, float y, float z) {
		this.nextMove.x += x;
		this.nextMove.y += y;
		this.nextMove.z += z;
	}
	
	public Vector3f getNextMove() {
		return nextMove;
	}
	
	public void resetNextMove() {
		this.nextMove.x = 0;
		this.nextMove.y = 0;
		this.nextMove.z = 0;
	}
}
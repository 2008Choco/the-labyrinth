package me.choco.labyrinth.utils;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import me.choco.labyrinth.entity.Entity;

public class MatrixMath {
	
	private static final Matrix4f projectionMatrix = new Matrix4f(), modelViewMatrix = new Matrix4f(), viewMatrix = new Matrix4f();
	
	public static Matrix4f getProjectionMatrix(float fov, float width, float height, float zNear, float zFar) {
		return projectionMatrix.identity().setPerspective(fov, width / height, zNear, zFar);
	}
	
	public static Matrix4f getModelViewMatrix(Entity entity, Matrix4f viewMatrix) {
		Vector3f rotation = entity.getRotation();
		
		modelViewMatrix.identity().translate(entity.getLocation())
			.rotateXYZ((float) Math.toRadians(-rotation.x), (float) Math.toRadians(-rotation.y), (float) Math.toRadians(-rotation.z))
			.scale(entity.getScale());
		
		Matrix4f currentView = new Matrix4f(viewMatrix);
		return currentView.mul(modelViewMatrix);
	}
	
	public static Matrix4f getViewMatrix(Camera camera) {
		Vector3f location = camera.getLocation();
		Vector3f rotation = camera.getRotation();
		
		return viewMatrix.identity()
			.rotateXYZ((float) Math.toRadians(rotation.x), (float) Math.toRadians(rotation.y), (float) Math.toRadians(rotation.z))
			.translate(-location.x, -location.y, -location.z);
	}
}
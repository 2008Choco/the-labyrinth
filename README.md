### General Information ###
* **Project Name:** The Labyrinth
* **Authors (2D):** Parker Hawke, Jordan Gavin
* **Authors (3D):** Parker Hawke
* **Date of Commencement (2D):** 6/10/2016 (June 10th, 2016)
* **Date of Commencement (3D):** 11/14/2016 (November 14th, 2016)

### What is this project ###
This is a collaborative project made by Parker Hawke and Jordan Gavin as an exemplar for future students in the Java programming course. The project is by no means meant to be an official game for release. The entire goal is to be a learning experience for other students. This code is free to use, modify, and manipulate to your pleasing, and no copyright claims are held to this code.

As the commencement of the 3D game as started, this code is also free to use. This is meant to be a game for a desire to learn the LWJGL library a bit better. The 3D project is being worked on by myself, Parker Hawke, exclusively without the help of any other person known personally. I will constantly be referring to a tutorial book to assist me in early development, but I will be partaking in solo development as I get more familiar with the API

### What does it do? ###
"Labyrinth" is to be a maze exploration game in which the goal is to make it to the exit point to move on to the next level. Along the way, there will be a few tricks and traps to try and catch the player off guard and throw them into the depths of the labyrinth. Teleportation pads may send you randomly in the maze or help you get closer to the exit. Trap floors may drop you to a lower floor in the maze. Who knows what lies inside. It might just be a death trap. Can *you* escape the labyrinth?

### Can I download the source code? ###
Of course, you can! If you are familiar with Git / Mercurial, you should know how to download this project. Otherwise, you are free to either do one of the following

##### Copying and Pasting #####
If you haven't used Git before, simply browse all the files, and copy and paste them into your IDE. Of course, you may have to change things such as the package name, or maybe even the class names. But remember if you are to change anything, refactor and change all references to said object as well.

##### Cloning the Repository #####
1. Download Atlassian SourceTree (Or your preferred git client / terminal)
2. Click on "Clone or Download" on the GitHub page in the top right
3. Select your file location to download a local copy of the repository
4. In SourceTree, click "Pull" and pull all the code from the repository (if any).
5. Open Eclipse IDE (Or your IDE of choice. However, this project is made using Eclipse)
6. Click "Import", and import "Projects from Git"
7. Feel free to program to your liking :D

### Intentions for 3D Games ###
I am starting fresh in 3D games, so there are bound to be many amounts of mistakes. The game engine is going to be custom from my own knowledge of Java and new knowledge of LWJGL 3. This game is most likely not going to get released on a high scale for any money. If I do decide to release this game, it will be for educational purposes and it will be released completely free of charge. Evidently, if my emotions about this game's progress change in the future, perhaps I will release it for a certain wager, but as of currently, there are no plans for such thing. I wish to complete this game by the end of the 2016/2017 school year, and perhaps use this as an exemplar when applying to universities such as University of Waterloo and UOIT. I hope to see interest from these universities as a thirst for knowledge and desire to learn game development in Software Engineering programs.
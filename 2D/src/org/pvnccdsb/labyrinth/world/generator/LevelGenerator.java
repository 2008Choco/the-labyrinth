package org.pvnccdsb.labyrinth.world.generator;

import java.util.Random;

import org.pvnccdsb.labyrinth.world.Level;
import org.pvnccdsb.labyrinth.world.Location;
import org.pvnccdsb.labyrinth.world.tile.TileHedge;

public class LevelGenerator {
	
	private static final Random random = new Random();
	
	private final Level level;
	private final long seed;
	private final SimplexNoiseGenerator generator;
	public LevelGenerator(Level level){
		this(level, random.nextLong());
	}
	
	public LevelGenerator(Level level, long seed){
		this.level = level;
		this.seed = seed;
		this.generator = new SimplexNoiseGenerator(seed);
	}
	
	/** Generate all tiles based on the size of the level (difficulty)
	 */
	public void generate(){
		for (int x = 0; x < level.getDifficulty().getWidth(); x++){
			for (int y = 0; y < level.getDifficulty().getHeight(); y++){
				double value = generator.eval(x, y);
				
				// TODO: This is not staying. It's way too weird of an algorithm. Needs to be fine tuned
				if (value < 0.0){
					Location location = new Location(x, y);
					level.setTile(location, new TileHedge(location, null));
				}
			}
		}
	}
	
	/** Get the level currently being generated
	 * @return the level
	 */
	public Level getLevel() {
		return level;
	}
	
	/** Get the seed used to generate the level
	 * @return the seed
	 */
	public long getSeed() {
		return seed;
	}
	
	/** Get the generator currently generating maze tiles
	 * @return the generator
	 */
	public SimplexNoiseGenerator getGenerator() {
		return generator;
	}
}
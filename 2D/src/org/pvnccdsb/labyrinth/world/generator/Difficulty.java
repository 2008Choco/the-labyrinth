package org.pvnccdsb.labyrinth.world.generator;

/** Used to determine the varying sizes of the levels (For random level generation)
 */
public enum Difficulty {
	
	EASY(20),
	MEDIUM(50),
	HARD(100);
	
	// TODO: A maximum watcher count per difficulty as well? Maybe...
	    // Those will get randomly located by the level generator as well
	
	private int width, height;
	private Difficulty(int size) {
		this(size, size);
	}
	
	private Difficulty(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/** Get the width of the map for the specified difficulty
	 * @return the width of the map
	 */
	public int getWidth() {
		return width;
	}
	
	/** Get the height of the map for the specified difficulty
	 * @return the height of the map
	 */
	public int getHeight() {
		return height;
	}
}
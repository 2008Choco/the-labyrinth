
package org.pvnccdsb.labyrinth.world;

import org.pvnccdsb.labyrinth.utils.Vector;

public class Location extends Vector {
	
	// Every "x" pixels is 1 coordinate
	public static final int PIXEL_RATIO = 32;
	
	public Location(float x, float y){
		super(x, y);
		this.x = x;
		this.y = y;
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public float getX(){
		return x;
	}
	
	public void setRawX(int x){
		this.x = x / PIXEL_RATIO;
	}
	
	public int getRawX(){
		return (int) (x * PIXEL_RATIO);
	}
	
	public int getTileX(){
		return (int) Math.floor(x);
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public float getY(){
		return y;
	}
	
	public void setRawY(int y){
		this.y = y / PIXEL_RATIO;
	}
	
	public int getRawY(){
		return (int) (y * PIXEL_RATIO);
	}
	
	public int getTileY(){
		return (int) Math.floor(y);
	}
	
	public double distance(Location location){
		return Math.sqrt(distanceSquared(location));
	}
	
	public double distanceSquared(Location location){
		return StrictMath.pow(getX() - location.getX(), 2) + StrictMath.pow(getY() - location.getY(), 2);
	}
	
	@Override
	public Location clone(){
		return new Location(x, y);
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Location)) return false;
		Location loc = (Location) object;
		return (x == loc.x && y == loc.y);
	}
	
	@Override
	public String toString() {
		return "Location:{x:" + getX() + ",y:" + getY() + ",rawX:" + getRawX() + ",rawY:" + getRawY() + "}";
	}
}
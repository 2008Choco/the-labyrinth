package org.pvnccdsb.labyrinth.world;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.pvnccdsb.labyrinth.world.generator.Difficulty;
import org.pvnccdsb.labyrinth.world.generator.LevelGenerator;
import org.pvnccdsb.labyrinth.world.tile.Tile;

public class Level {
	
	private List<Tile> tiles = new ArrayList<>();
	
	private final UUID uuid;
	private final Difficulty difficulty;
	
	public Level(){
		this(Difficulty.EASY);
	}
	
	public Level(Difficulty difficulty){
		this.uuid = UUID.randomUUID();
		this.difficulty = difficulty;
		
		new LevelGenerator(this).generate();
	}
	
	/** Get the UUID of the level to be stored in file
	 * @return the UUID of the level
	 */
	public UUID getUniqueId(){
		return uuid;
	}
	
	/** Get the difficulty of the level
	 * @return the level difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}
	
	/** Set a tile at a specific location
	 * @param location - The location to set
	 * @param tile - The tile to set
	 */
	public void setTile(Location location, Tile tile){
		Tile tileAt = getTileAt(location);
		if (tileAt != null || (tileAt != null && tile == null)) 
			tiles.remove(tileAt);
		tiles.add(tile);
	}
	
	/** Get a tile at the specified location
	 * @param location - The location
	 * @return the tile
	 */
	public Tile getTileAt(Location location){
		for (Tile tile : tiles)
			if (tile.getLocation().equals(location)) return tile;
		return null;
	}
	
	/** Get all current tiles in the level */
	public List<Tile> getTiles() {
		return tiles;
	}
	
	/** Called 60 times per second to update the level */
	public void tick(){
		Iterator<Tile> it = tiles.iterator();
		while (it.hasNext()){
			Tile tile = it.next();
			tile.tick();
		}
	}
	
	/** Called every time the level requires rendering 
	 * @param g - The graphical context to draw to
	 */
	public void render(Graphics g){
		Iterator<Tile> it = tiles.iterator();
		while (it.hasNext()){
			Tile tile = it.next();
			tile.render(g);
		}
	}
}
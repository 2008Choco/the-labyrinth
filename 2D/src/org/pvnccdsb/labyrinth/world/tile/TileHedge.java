package org.pvnccdsb.labyrinth.world.tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.pvnccdsb.labyrinth.world.Location;

public class TileHedge extends Tile {

	public TileHedge(Location location, BufferedImage sprite) {
		super(location, sprite);
	}
	
	@Override
	public boolean isTraversable() {
		return false;
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(location.getRawX(), location.getRawY(), width, height);
	}
}
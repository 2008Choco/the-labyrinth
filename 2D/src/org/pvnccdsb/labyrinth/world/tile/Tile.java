package org.pvnccdsb.labyrinth.world.tile;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import org.pvnccdsb.labyrinth.utils.ICollidable;
import org.pvnccdsb.labyrinth.world.Location;

public abstract class Tile implements ICollidable{
	
	protected boolean loaded = true;
	
	protected int width = 32, height = 32;
	
	protected Location location;
	protected BufferedImage sprite;
	public Tile(Location location, BufferedImage sprite){
		this.location = location;
		this.sprite = sprite;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public BufferedImage getSprite() {
		return sprite;
	}
	
	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
	
	@Override
	public Rectangle getBounds(){
		return new Rectangle(location.getRawX(), location.getRawY(), width, height);
	}
	
	@Override
	public Rectangle getBoundsTop(){
		return new Rectangle(location.getRawX() + 5, location.getRawY(), width - 10, 5);
	}
	
	@Override
	public Rectangle getBoundsLeft(){
		return new Rectangle(location.getRawX(), location.getRawY() + 1, 5, height - 2);
	}
	
	@Override
	public Rectangle getBoundsRight(){
		return new Rectangle((location.getRawX() + width) - 5, location.getRawY() + 1, 5, height - 2);
	}
	
	@Override
	public Rectangle getBoundsDown(){
		return new Rectangle(location.getRawX() + 5, (location.getRawY() + height) - 5, width - 10, 5);
	}
	
	/** Called 60 times per second to update the tile */
	public void tick(){
		
	}
	
	/** Called every time the level requires rendering 
	 * @param g - The graphical context to draw to
	 */
	public void render(Graphics g){
		if (!loaded) return;
		g.drawImage(sprite, location.getRawX(), location.getRawY(), null);
	}
	
	public abstract boolean isTraversable();
}
package org.pvnccdsb.labyrinth.entity;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.UUID;

import org.pvnccdsb.labyrinth.Labyrinth;
import org.pvnccdsb.labyrinth.utils.ICollidable;
import org.pvnccdsb.labyrinth.utils.Vector;
import org.pvnccdsb.labyrinth.utils.manager.EntityManager;
import org.pvnccdsb.labyrinth.world.Location;

public abstract class Entity implements ICollidable {
	
	protected static final EntityManager manager = Labyrinth.getLabyrinth().getEntityManager();
	
	protected final UUID uuid = UUID.randomUUID();
	protected Location location;
	protected Vector velocity;
	protected int width, height;
	protected BufferedImage sprite;
	
	/** The base abstract class to create a new Entity. 
	 * <br>This class cannot be instantiated on its own, its implementing classes must be instantiated
	 * @param location - The location of the entity
	 * @param width - The width of the entity
	 * @param height - The height of the entity
	 * @param sprite - The sprite of the entity
	 */
	protected Entity(Location location, int width, int height, BufferedImage sprite) {
		this.location = location;
		this.width = width;
		this.height = height;
		this.sprite = sprite;
		
		this.velocity = new Vector(0, 0);
	}
	
	public void setVelocity(Vector velocity) {
		this.velocity = velocity;
	}
	
	public Vector getVelocity() {
		return velocity;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public UUID getUniqueId(){
		return uuid;
	}

	public BufferedImage getSprite() {
		return sprite;
	}
	
	@Override
	public Rectangle getBounds(){
		return new Rectangle(location.getRawX(), location.getRawY(), width, height);
	}
	
	@Override
	public Rectangle getBoundsTop(){
		return new Rectangle(location.getRawX() + 1, location.getRawY(), width - 3, 5);
	}
	
	@Override
	public Rectangle getBoundsLeft(){
		return new Rectangle(location.getRawX(), location.getRawY() + 1, 5, height - 3);
	}
	
	@Override
	public Rectangle getBoundsRight(){
		return new Rectangle((location.getRawX() + width) - 6, location.getRawY() + 1, 5, height - 3);
	}
	
	@Override
	public Rectangle getBoundsDown(){
		return new Rectangle(location.getRawX() + 1, (location.getRawY() + height) - 6, width - 3, 5);
	}
	
	/** Called 60 times per second to update the entity */
	public void tick(){
		location.add(velocity); // Increment the location by x velocity
	}
	
	/** Called every time the entity requires rendering 
	 * @param g - The graphical context to draw to
	 */
	public void render(Graphics g){
		g.drawImage(sprite, location.getRawX(), location.getRawY(), width, height, null);
	}
}
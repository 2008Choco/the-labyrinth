package org.pvnccdsb.labyrinth.entity;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.pvnccdsb.labyrinth.entity.ai.PathfinderGoalAvoid;
import org.pvnccdsb.labyrinth.utils.ICollidable;
import org.pvnccdsb.labyrinth.world.Location;

/** <b>The Watcher:</b>
 * <br>A creature that will follow the player throughout the maze
 */
public class Watcher extends IntelligentEntity implements ICollidable {
	
	/** Constructor to create a new Watcher instance. This is a client-sided Watcher object
	 * <br>Each Watcher has its own width and height of 32
	 * @param location - The location of the entity
	 * @param sprite - The sprite of the entity
	 */
	public Watcher(Location location, BufferedImage sprite) {
		super(location, 32, 32, sprite);
		
		// this.goals.add(new PathfinderGoalFollowPlayer(this, 10));
		this.goals.add(new PathfinderGoalAvoid(this, Player.class, 3));
	}
	
	@Override
	public void tick() {
		super.tick();
		
		PathfinderGoalAvoid pga = (PathfinderGoalAvoid) getPathfinderGoal(PathfinderGoalAvoid.class);
		if (pga != null) pga.searchForNearbyEntity();
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(location.getRawX(), location.getRawY(), width, height);
	}
}
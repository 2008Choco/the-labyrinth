package org.pvnccdsb.labyrinth.entity.variants;

import java.awt.event.KeyEvent;

import org.pvnccdsb.labyrinth.utils.listener.KeyboardListener;

public interface IControllable {
	
	/** Called by the KeyboardListener when a key is pressed
	 * @param event - The event that was fired
	 * @param listener - The listener that fired the event
	 */
	public void onPressKey(KeyEvent event, KeyboardListener listener);
	
	/** Called by the KeyboardListener when a key is released
	 * @param event - The event that was fired
	 * @param listener - The listener that fired the event
	 */
	public void onReleaseKey(KeyEvent event, KeyboardListener listener);
	
}
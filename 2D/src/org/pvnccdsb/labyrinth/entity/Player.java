package org.pvnccdsb.labyrinth.entity;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import org.pvnccdsb.labyrinth.entity.variants.IControllable;
import org.pvnccdsb.labyrinth.utils.listener.KeyboardListener;
import org.pvnccdsb.labyrinth.world.Location;

/** <b>The Player:</b>
 * <br>The entity to be controlled by the user
 */
public class Player extends Entity implements IControllable {
	
	private static final float maxSpeed = 0.05f;

	/** Constructor to create a new Player instance. This is a client-sided Player object
	 * <br>Each player has its own width and height of 32
	 * @param location - The location of the entity
	 * @param sprite - The sprite of the entity
	 */
	public Player(Location location, BufferedImage sprite) {
		super(location, 32, 32, sprite);
	}

	@Override
	public void onPressKey(KeyEvent event, KeyboardListener listener) {
		if(listener.isKeyPressed(KeyEvent.VK_W)){
			velocity.setY(-maxSpeed);
		}
		if(listener.isKeyPressed(KeyEvent.VK_S)){
			velocity.setY(maxSpeed);
		}
		if(listener.isKeyPressed(KeyEvent.VK_A)){
			velocity.setX(-maxSpeed);
		}
		if(listener.isKeyPressed(KeyEvent.VK_D)){
			velocity.setX(maxSpeed);
		}
	}

	@Override
	public void onReleaseKey(KeyEvent event, KeyboardListener listener) {
		if(listener.isKeyReleased(KeyEvent.VK_W)){
			velocity.setY(0);
			onPressKey(event, listener);
		}
		if(listener.isKeyReleased(KeyEvent.VK_S)){
			velocity.setY(0);
			onPressKey(event, listener);
		}
		if(listener.isKeyReleased(KeyEvent.VK_A)){
			velocity.setX(0);
			onPressKey(event, listener);
		}
		if(listener.isKeyReleased(KeyEvent.VK_D)){
			velocity.setX(0);
			onPressKey(event, listener);
		}
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.BLUE);
		g.fillRect(location.getRawX(), location.getRawY(), width, height);
	}
}
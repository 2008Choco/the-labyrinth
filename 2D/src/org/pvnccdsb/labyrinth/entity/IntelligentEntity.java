package org.pvnccdsb.labyrinth.entity;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.pvnccdsb.labyrinth.entity.ai.PathfinderGoal;
import org.pvnccdsb.labyrinth.world.Location;

public abstract class IntelligentEntity extends Entity {
	
	protected final List<PathfinderGoal> goals = new ArrayList<>();

	protected IntelligentEntity(Location location, int width, int height, BufferedImage sprite) {
		super(location, width, height, sprite);
	}
	
	/** Get the pathfinder goals currently operated by the IntelligentEntity
	 * @return The set of goals
	 */
	public List<PathfinderGoal> getPathfinderGoals(){
		return goals;
	}
	
	/** Check whether the IntelligentEntity has the specified pathfinder goal or not
	 * @param goal - The pathfinder goal class to search for
	 * @return if the player has the pathfinder goal
	 */
	public boolean hasPathfinderGoal(Class<? extends PathfinderGoal> goal){
		if (goal.equals(PathfinderGoal.class)) throw new IllegalArgumentException("Cannot search for pathfinder base");
		for (PathfinderGoal g : goals)
			if (g.getClass().equals(goal)) return true;
		return false;
	}
	
	/** Get an instance of a pathfinder goal currently operated by the IntelligentEntity.
	 * Will return null if no goal is found
	 * @param goal - The pathfinder goal class to search for
	 * @return an instance of the specified pathfinder goal
	 */
	public PathfinderGoal getPathfinderGoal(Class<? extends PathfinderGoal> goal){
		return getPathfinderGoal(goal, 0);
	}
	
	/** Get an instance of a pathfinder goal currently operated by the IntelligentEntity at the index specified 
	 * (index of the pathfinder goal, not of the list of pathfinder goals). Will return null if no goal is found
	 * @param goal - The pathfinder goal class to search for
	 * @param index - the index id of the specified goal
	 * @return an instance of the specified pathfinder goal
	 */
	public PathfinderGoal getPathfinderGoal(Class<? extends PathfinderGoal> goal, int index){
		if (index >= goals.size()) throw new IllegalArgumentException("Index cannot exceed the size of the pathfinder goal list");
		if (goal.equals(PathfinderGoal.class)) throw new IllegalArgumentException("Cannot search for pathfinder base");
		
		int currentIndex = 0;
		for (PathfinderGoal g : goals){
			if (g.getClass().equals(goal)){
				if (currentIndex == index) return g;
				currentIndex++;
			}
		}
		return null;
	}
	
	@Override
	public void tick() {
		super.tick();
		
		// Pathfinder goal execution
		for (PathfinderGoal goal : goals)
			if (goal.shouldExecute()) goal.execute();
	}
}
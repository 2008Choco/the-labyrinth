package org.pvnccdsb.labyrinth.entity.ai;

import java.util.List;

import org.pvnccdsb.labyrinth.Labyrinth;
import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.entity.IntelligentEntity;
import org.pvnccdsb.labyrinth.utils.manager.EntityManager;
import org.pvnccdsb.labyrinth.world.Location;

/** Goal: Avoid a specified entity around a specific radius whilst avoiding obstacles
 */
public class PathfinderGoalAvoid extends PathfinderGoal {
	
	private static final EntityManager manager = Labyrinth.getLabyrinth().getEntityManager();
	
	private final Entity avoider;
	private double radiusSquared;
	
	private Class<? extends Entity> toAvoid;
	private Entity avoiding;
	
	/** Create a new PathfinderGoalAvoid instance. Will avoid all entities of type specified
	 * @param avoider - The entity doing the avoiding
	 * @param toAvoid - The entity type to avoid
	 * @param radius - The range to avoid
	 */
	public PathfinderGoalAvoid(Entity avoider, Class<? extends Entity> toAvoid, double radius){
		if (avoider == null) throw new IllegalArgumentException("Entity to move cannot be null");
		if (toAvoid == null) throw new IllegalArgumentException("Entity class to avoid cannot be null");
		if (radius < 0) throw new IllegalArgumentException("Cannot specify a negative range");
		
		this.avoider = avoider;
		this.toAvoid = toAvoid;
		this.radiusSquared = radius * radius;
	}
	
	/** Get the entity currently avoiding
	 * @return the entity
	 */
	public Entity getAvoider() {
		return avoider;
	}
	
	/** Set the entity currently being avoided
	 * @param toAvoid - The entity to avoid
	 */
	public void setToAvoid(Entity toAvoid){
		this.avoiding = toAvoid;
	}
	
	/** Get the entity currently being avoided
	 * @return the entity being avoided
	 */
	public Entity getAvoiding(){
		return avoiding;
	}
	
	/** Get the entity class currently being avoided
	 * @return the entity class being avoided
	 */
	public Class<? extends Entity> getToAvoid() {
		return toAvoid;
	}
	
	/** Set the radius to avoid the entity
	 * @param radius - The radius
	 */
	public void setRadius(int radius) {
		this.radiusSquared = radius * radius;
	}
	
	/** Get the radius to avoid the entity
	 * @return the radius
	 */
	public double getRadius() {
		return radiusSquared / radiusSquared;
	}
	
	/** Search for a nearby entity to avoid. Required to be called in {@link IntelligentEntity#tick()} method
	 */
	public void searchForNearbyEntity(){
		List<Entity> entities = manager.getEntities(toAvoid);
		entities.sort((entity, entity2) -> {
			if (entity.getLocation().distanceSquared(avoider.getLocation()) > entity2.getLocation().distanceSquared(avoider.getLocation())) return 1;
			if (entity.getLocation().distanceSquared(avoider.getLocation()) < entity2.getLocation().distanceSquared(avoider.getLocation())) return -1;
			return 0;
		});
		avoiding = entities.stream().filter(e -> !e.equals(avoider)).findFirst().orElse(null);
	}

	@Override
	public boolean shouldExecute(){
		if (avoiding == null) return false;
		
		boolean execute = avoider.getLocation().distanceSquared(avoiding.getLocation()) < radiusSquared;
		if (!execute){
			avoider.getVelocity().setX(0);
			avoider.getVelocity().setY(0);
		}
		return execute;
	}

	@Override
	public void execute(){
		Location avoiderLoc = avoider.getLocation().clone(), toAvoidLoc = avoiding.getLocation().clone();
		avoider.setVelocity(avoiderLoc.subtract(toAvoidLoc).normalize().divide(15));
	}
}
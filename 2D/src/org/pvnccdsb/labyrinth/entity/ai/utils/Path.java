package org.pvnccdsb.labyrinth.entity.ai.utils;

import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.world.Level;
import org.pvnccdsb.labyrinth.world.Location;

public class Path {
	
	private final Entity entity;
	private final Level level;
	private final Location destination;
	public Path(Entity entity, Level level, Location destination){
		this.entity = entity;
		this.level = level;
		this.destination = destination;
	}
	
	public void executePath(){
		if (!entity.getLocation().equals(destination)){
			@SuppressWarnings("unused")
			Location nextLocation = findBestPathTo(level, destination);
			// TODO: Move entity to nextLocation
		}
	}
	
	public static Location findBestPathTo(Level level, Location location){
		// TODO: Algorithm to find the next best move to get to a specific destination
		return null;
	}
}
package org.pvnccdsb.labyrinth.entity.ai;

import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.entity.Player;

/** Goal: Follow a specified player around the level whilst avoiding obstacles
 */
public class PathfinderGoalFollowPlayer extends PathfinderGoal {
	
	private Player player;
	private final Entity follower;
	private final int radiusSquared;
	
	/** Create a new PathfinderGoalFollowPlayer instance.
	 * @param follower - The entity doing the following
	 * @param player - The player to follow
	 * @param radius - The range to follow
	 */
	public PathfinderGoalFollowPlayer(Entity follower, Player player, int radius){
		if (follower == null) throw new IllegalArgumentException("Entity to move cannot be null");
		if (player == null) throw new IllegalArgumentException("Player to follow cannot be null");
		if (radius < 0) throw new IllegalArgumentException("Cannot specify a negative range");
		
		this.follower = follower;
		this.player = player;
		this.radiusSquared = radius != -1 ? radius * radius : -1; // Just to make sure -1 is possible
	}
	
	/** Create a new PathfinderGoalFollowPlayer instance.
	 * The unspecified player will result in null. See {@link #setPlayer(Player)}
	 * @param follower - The entity doing the following
	 * @param range - The range to follow
	 */
	public PathfinderGoalFollowPlayer(Entity follower, int range){
		this(follower, null, range);
	}
	
	/** Create a new PathfinderGoalFollowPlayer instance.
	 * The unspecified radius will result in an infinite follow radius
	 * @param follower - The entity doing the following
	 * @param player - The player to follow
	 */
	public PathfinderGoalFollowPlayer(Entity follower, Player player){
		this(follower, player, -1);
	}
	
	/** Create a new PathfinderGoalFollowPlayer instance.
	 * The unspecified player will result in null. See {@link #setPlayer(Player)}
	 * The unspecified radius will result in an infinite follow radius
	 * @param follower - The entity doing the following
	 */
	public PathfinderGoalFollowPlayer(Entity follower){
		this(follower, null, -1);
	}
	
	/** Set the player currently being followed
	 * @param player - The player to follow
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	/** Get the player currently being followed
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}
	
	/** Get the entity current following
	 * @return the entity
	 */
	public Entity getFollower() {
		return follower;
	}
	
	/** Get the range in which this pathfinder goal is searching */
	public int getRange() {
		return radiusSquared / radiusSquared;
	}

	@Override
	public boolean shouldExecute() {
		// Distance = SQRT((x2-x1)^2+(y2-y1)^2)
		if (player == null) return false;
		if (radiusSquared == -1) return true;
		return follower.getLocation().distanceSquared(player.getLocation()) < radiusSquared;
	}

	@Override
	public void execute() {
		// TODO: STUB METHOD
	}
}
package org.pvnccdsb.labyrinth.entity.ai;

public abstract class PathfinderGoal {
	
	public enum PathfinderResult{
		WAITING, IN_PROGRESS, SUCCESS, FAILED;
	}
	
	protected PathfinderResult result = PathfinderResult.WAITING;
	
	public void setResult(PathfinderResult result) {
		this.result = result;
	}
	
	public PathfinderResult getResult() {
		return result;
	}
	
	/** Whether this pathfinder goal should execute
	 * @return True if should execute
	 */
	public abstract boolean shouldExecute();
	
	/** The actual execution of the pathfinder goal */
	public abstract void execute();
}
package org.pvnccdsb.labyrinth.entity.ai;

import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.world.Location;

/** Goal: Follow a specific path to get to a location whilst avoiding obstacles
 */
public class PathfinderGoalMoveTo extends PathfinderGoal {
	
	private final Entity entity;
	private final Location to;
	
	/** Create a new PathfinderGoalMoveTo instance
	 * @param entity - The entity moving
	 * @param to - The location to move to
	 */
	public PathfinderGoalMoveTo(Entity entity, Location to){
		if (entity == null) throw new IllegalArgumentException("Entity to move cannot be null");
		if (to == null) throw new IllegalArgumentException("Destination cannot be null");
		
		this.entity = entity;
		this.to = to;
	}
	
	/** Get the entity currently moving
	 * @return The entity
	 */
	public Entity getEntity() {
		return entity;
	}
	
	/** Get the location currently being moved to
	 * @return The location
	 */
	public Location getTo() {
		return to;
	}

	@Override
	public boolean shouldExecute() {
		return true;
	}

	@Override
	public void execute() {
		// TODO: STUB METHOD
	}
}
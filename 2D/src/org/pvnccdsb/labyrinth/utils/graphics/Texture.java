package org.pvnccdsb.labyrinth.utils.graphics;

import java.awt.image.BufferedImage;

public class Texture {
	
	/* Entities */
	public static final BufferedImage PLAYER = null;
	public static final BufferedImage WATCHER = null;
	
	/* Tiles */
	public static final BufferedImage TILE_HEDGE = null;
	
	/* General */
	// public static final BufferedImage GENERAL_TEXTURE_NAME = null;
	
	public static void loadTextures(){
		// TODO: LOAD ALL TEXTURES
	}
	
	// Non-initializable class
	private Texture(){};
}
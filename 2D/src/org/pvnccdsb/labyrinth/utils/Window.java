package org.pvnccdsb.labyrinth.utils;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.URI;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.pvnccdsb.labyrinth.Labyrinth;

public class Window{
	
	public Window(int width, int height, String title, Labyrinth game){
		checkJavaVersion(1.8);
		
		JFrame frame = new JFrame(title + " - " + Labyrinth.version);
		
		frame.setSize(width, height);
		frame.setMinimumSize(new Dimension(width, height));
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setEnabled(true);
		
		frame.addComponentListener(new ComponentListener(){
			public void componentResized(ComponentEvent event){
				if (event.getComponent().getWidth() == Labyrinth.WIDTH && event.getComponent().getHeight() == Labyrinth.HEIGHT) return;
				Labyrinth.WIDTH = event.getComponent().getWidth();
				Labyrinth.HEIGHT = event.getComponent().getHeight();
			}
			
			public void componentHidden(ComponentEvent event) {}
			public void componentMoved(ComponentEvent event) {}
			public void componentShown(ComponentEvent event) {}
		});
		
		frame.addWindowListener(new WindowListener(){
			@Override
			public void windowClosing(WindowEvent event){
				game.setRunning(false);
			}

			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
		
		frame.add(game);
		game.createBufferStrategy(3);
		game.requestFocus();
		game.start();
	}
	
	private void checkJavaVersion(double requiredVersion){
		if (getJavaVersion() < requiredVersion){
			Object[] options = {"Do it later","Download Java " + requiredVersion};
			int choice = JOptionPane.showOptionDialog(null, "Java version " + getJavaVersion() + " detected!\n" + "Make sure you have JRE " + requiredVersion + " installed! This game will not work otherwise", 
					"Invalid Java Runtime Environment!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, null);
			if (choice == 1){
				try {Desktop.getDesktop().browse(URI.create("http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html"));}
				catch (IOException e){e.printStackTrace();}
			}
			System.exit(1);
		}
	}
	
	private static double getJavaVersion(){
	    String version = System.getProperty("java.version");
	    int pos = version.indexOf('.');
	    pos = version.indexOf('.', pos+1);
	    return Double.parseDouble (version.substring (0, pos));
	}
}
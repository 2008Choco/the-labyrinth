package org.pvnccdsb.labyrinth.utils.manager;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.world.Location;

public class EntityManager {
	
	private final Set<Entity> entities = new HashSet<>();
	
	/** Add a new entity to the game
	 * @param entity - The entity to add
	 */
	public void addEntity(Entity entity){
		this.entities.add(entity);
	}
	
	/** Remove an entity from the game
	 * @param entity - The entity to remove
	 */
	public void removeEntity(Entity entity){
		this.entities.remove(entity);
	}
	
	/** Get all entities currently registered and added to the world
	 * @return A Set of all entities
	 */
	public Set<Entity> getEntities(){
		return entities;
	}
	
	/** Get all entities currently registered and added to the world of a specified type
	 * @param type - The type of entity to get
	 * @return A list of all entities of the specified type
	 */
	public List<Entity> getEntities(Class<? extends Entity> type){
		List<Entity> entities = new ArrayList<>();
		for (Entity entity : this.entities)
			if (entity.getClass().equals(type)) entities.add(entity);
		return entities;
	}
	
	/** Get the first entity in the list of the type specified
	 * @param type - The type of entity to search
	 * @return The first entity. Null if not found
	 */
	public Entity getFirst(Class<? extends Entity> type){
		for (Entity entity : this.entities)
			if (entity.getClass().equals(type)) return entity;
		return null;
	}
	
	/** Get an entity by UUID
	 * @param uuid - The UUID to search for
	 * @return The entity. Null if not found
	 */
	public Entity getEntityByUniqueId(UUID uuid) {
		for(Entity entity : this.entities)
			if(entity.getUniqueId().equals(uuid)) return entity;
		return null;
	}
	
	/** Get an entity by UUID (String variation)
	 * @param uuid - The UUID to search for
	 * @return The entity. Null if not found
	 */
	public Entity getEntityByUniqueId(String uuid) {
		return getEntityByUniqueId(UUID.fromString(uuid));
	}
	
	/** Tick every single entity currently available in the world */
	public void tick(){
		Iterator<Entity> it = entities.iterator();
		while (it.hasNext()){
			Entity entity = it.next();
			entity.tick();
			
			// Entity Collision
			for (Entity collided : entities){
				if (collided.equals(entity) || entity.getLocation().distanceSquared(collided.getLocation()) > 10) continue;
				if (!entity.collidesWith(collided)) continue;

				if (entity.collidesTop(collided)){
					entity.getLocation().setY(collided.getLocation().getY() + ((float) collided.getHeight() / (float) Location.PIXEL_RATIO));
					entity.getVelocity().setY(0);
				}else if (entity.collidesDown(collided)){
					entity.getLocation().setY(collided.getLocation().getY() - ((float) entity.getHeight() / (float) Location.PIXEL_RATIO));
					entity.getVelocity().setY(0);
				}
				
				if (entity.collidesLeft(collided)){
					entity.getLocation().setX(collided.getLocation().getX() + ((float) collided.getWidth() / (float) Location.PIXEL_RATIO));
					entity.getVelocity().setX(0);
				}else if (entity.collidesRight(collided)){
					entity.getLocation().setX(collided.getLocation().getX() - ((float) entity.getWidth() / (float) Location.PIXEL_RATIO));
					entity.getVelocity().setX(0);
				}
			}
		}
	}
	
	/** Render every single entity currently available in the world
	 * @param g - The graphical context to draw to
	 */
	public void render(Graphics g){
		Iterator<Entity> it = entities.iterator();
		while (it.hasNext()){
			Entity entity = it.next();
			entity.render(g);
		}
	}
}
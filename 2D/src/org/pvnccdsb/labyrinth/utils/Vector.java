package org.pvnccdsb.labyrinth.utils;

/* 
 * http://natureofcode.com/book/chapter-1-vectors/
 * 
 * This is a fantastic tutorial on Vectors and how they work. 
 * This should guide you through how to get vectors working, and I highly recommend giving it a quick read
 */

/** A scalar unit containing information about magnitude and direction.
 * Used to control the movement, acceleration and velocity of entities as well as positioning along the coordinate grid
 */
public class Vector {
	
	protected float x, y;
	public Vector() {
		this.x = 0.0f;
		this.y = 0.0f;
	}
	
	public Vector(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/** Set the x coordinate of this vector
	 * @param x - The value to set
	 */
	public void setX(float x) {
		this.x = x;
	}
	
	/** Get the x coordinate of this vector
	 * @retrun the x coordinate
	 */
	public float getX() {
		return x;
	}
	
	/** Set the y coordinate of this vector
	 * @param y - The value to set
	 */
	public void setY(float y) {
		this.y = y;
	}
	
	/** Get the y coordinate of this vector
	 * @retrun the y coordinate
	 */
	public float getY() {
		return y;
	}
	
	/** Add a vector to the current vector
	 * @param vector - The vector to add
	 */
	public Vector add(Vector vector){
		this.x += vector.x;
		this.y += vector.y;
		return this;
	}
	
	/** Subtract a vector from the current vector
	 * @param vector - The vector to subtract
	 */
	public Vector subtract(Vector vector){
		this.x -= vector.x;
		this.y -= vector.y;
		return this;
	}
	
	/** Multiply the magnitude of the vector by the specified scalar (value)
	 * @param scalar - the value to multiply by
	 */
	public Vector multiply(float scalar){
		this.x *= scalar;
		this.y *= scalar;
		return this;
	}
	
	/** Divide the magnitude of the vector by the specified scalar (value)
	 * @param scalar - the value to divide by
	 */
	public Vector divide(float scalar){
		this.x /= scalar;
		this.y /= scalar;
		return this;
	}
	
	/** Get the magnitude (length) of the vector
	 * @return the magnitude of the vector
	 */
	public double getMagnitude(){
		return Math.sqrt(getMagnitudeSquared());
	}
	
	/** Get the magnitude (length) of the vector squared
	 * @see #getMagnitude()
	 * @return the length of the vector squared
	 */
	public double getMagnitudeSquared(){
		return (x * x) + (y * y);
	}
	
	/** Normalize the vector, resulting in a unit vector
	 */
	public Vector normalize(){
		double magnitude = getMagnitude();
		if (magnitude != 0){
			divide((float) magnitude);
		}
		return this;
	}
	
	/** Limit the vectors magnitude to the scalar specified
	 * @param scalar - the maximum length
	 */
	public void limit(float scalar){
		if (getMagnitudeSquared() > (scalar * scalar)){ // Using squared function for CPU usage reasons
			normalize();
			multiply(scalar);
		}
	}
	
	@Override
	public Vector clone() {
		return new Vector(x, y);
	}
	
	@Override
	public String toString() {
		return "Vector:{x" + x + ",y:" + y + "}";
	}
}
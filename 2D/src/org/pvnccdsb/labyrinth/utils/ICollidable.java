package org.pvnccdsb.labyrinth.utils;

import java.awt.Rectangle;

public interface ICollidable {
	
	/** Get the bounds the ICollidable takes up
	 * @return The bounds
	 */
	public Rectangle getBounds();
	
	/** Get the bounds the ICollidable takes up on the top side
	 * @return The bounds on the top
	 */
	public Rectangle getBoundsTop();
	
	/** Get the bounds the ICollidable takes up on the left side
	 * @return The bounds on the left
	 */
	public Rectangle getBoundsLeft();
	
	/** Get the bounds the ICollidable takes up on the right side
	 * @return The bounds on the right
	 */
	public Rectangle getBoundsRight();
	
	/** Get the bounds the ICollidable takes up on the bottom side
	 * @return The bounds on the bottom
	 */
	public Rectangle getBoundsDown();
	
	/** Whether the ICollidable's bounds intersect with another ICollidable
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public default boolean collidesWith(ICollidable entity){
		return getBounds().intersects(entity.getBounds());
	}
	
	/** Whether the ICollidable's top bounds intersect with another ICollidable's bottom bounds
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public default boolean collidesTop(ICollidable entity){
		return getBoundsTop().intersects(entity.getBoundsDown());
	}
	
	/** Whether the ICollidable's left bounds intersect with another ICollidable's right bounds
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public default boolean collidesLeft(ICollidable entity){
		return getBoundsLeft().intersects(entity.getBoundsRight());
	}
	
	/** Whether the ICollidable's right bounds intersect with another ICollidable's left bounds
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public default boolean collidesRight(ICollidable entity){
		return getBoundsRight().intersects(entity.getBoundsLeft());
	}
	
	/** Whether the ICollidable's bottom bounds intersect with another ICollidable's top bounds
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public default boolean collidesDown(ICollidable entity){
		return getBoundsDown().intersects(entity.getBoundsTop());
	}
	
}
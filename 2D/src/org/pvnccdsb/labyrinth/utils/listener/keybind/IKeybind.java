package org.pvnccdsb.labyrinth.utils.listener.keybind;

public interface IKeybind {
	
	/** Get the keys required to fire the keybind
	 * @return an array of the keys
	 */
	public int[] getRequiredKeys();
	
	/** The method called when the keystroke is successfully pressed */
	public void execute();
	
}
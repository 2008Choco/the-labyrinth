package org.pvnccdsb.labyrinth.utils.listener.keybind;

import java.awt.event.KeyEvent;

import org.pvnccdsb.labyrinth.Labyrinth.Debug;

public class DebugToggleKeybind implements IKeybind {
	
	private static final int[] requiredKeys = {KeyEvent.VK_CONTROL, KeyEvent.VK_D};
	
	@Override
	public int[] getRequiredKeys() {
		return requiredKeys;
	}
	
	@Override
	public void execute() {
		Debug.DISPLAY_FTPS = !Debug.DISPLAY_FTPS;
	}
}
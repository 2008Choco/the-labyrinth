package org.pvnccdsb.labyrinth.utils.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.pvnccdsb.labyrinth.Labyrinth;
import org.pvnccdsb.labyrinth.Labyrinth.GameState;
import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.entity.variants.IControllable;
import org.pvnccdsb.labyrinth.utils.listener.keybind.DebugToggleKeybind;
import org.pvnccdsb.labyrinth.utils.listener.keybind.IKeybind;
import org.pvnccdsb.labyrinth.utils.manager.EntityManager;

public class KeyboardListener implements KeyListener{
	
	private static IKeybind[] keybinds;
	
	private final boolean[] keys = new boolean[1024];
	
	private Labyrinth game;
	private EntityManager manager;
	public KeyboardListener(Labyrinth game){
		this.game = game;
		this.manager = game.getEntityManager();
		
		// Register all keybinds after this line
		keybinds = new IKeybind[]{
			new DebugToggleKeybind()
		};
	}
	
	@Override
	public void keyPressed(KeyEvent event) {
		keys[event.getKeyCode()] = true;
		
		// Keybind checks
		boolean kbPress = false;
		for (IKeybind keybind : keybinds){
			if (areKeysPressed(keybind.getRequiredKeys())){
				keybind.execute();
				kbPress = true;
				break;
			}
		}
		
		// Entity key checks
		if (kbPress) return;
		for (Entity entity : manager.getEntities()){
			if (!game.getState().equals(GameState.GAME)) return;
			if (entity instanceof IControllable)
				((IControllable) entity).onPressKey(event, this);
		}
	}
	
	@Override
	public void keyReleased(KeyEvent event) {
		keys[event.getKeyCode()] = false;
		
		for (Entity entity : manager.getEntities()){
			if (!game.getState().equals(GameState.GAME)) return;
			if (entity instanceof IControllable)
				((IControllable) entity).onReleaseKey(event, this);
		}
	}
	
	public void keyTyped(KeyEvent e){ /* Null method. Here to stop KeyListener from yelling at me */ }
	
	/** Check whether a key is pressed or not
	 * @param key - The key to check
	 * @return true if the key is pressed
	 */
	public boolean isKeyPressed(int key){
		return keys[key];
	}
	
	/** Check whether a sequence of keys are pressed
	 * @param keys - The keys to check
	 * @return true if all keys are pressed
	 */
	public boolean areKeysPressed(int... keys){
		boolean allPressed = true;
		for (int key : keys)
			if (isKeyReleased(key)) allPressed = false;
		return allPressed;
	}
	
	/** Check whether a key is pressed or not
	 * @param key - The key to check
	 * @return true if the key is released
	 */
	public boolean isKeyReleased(int key){
		return !keys[key];
	}
	
	/** Check whether a sequnce of keys are released
	 * @param keys - The keys to check
	 * @return true if all keys are released
	 */
	public boolean areKeysReleased(int... keys){
		boolean allReleased = true;
		for (int key : keys)
			if (isKeyPressed(key)) allReleased = false;
		return allReleased;
	}
}
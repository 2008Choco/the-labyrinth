package org.pvnccdsb.labyrinth;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

import org.pvnccdsb.labyrinth.entity.Entity;
import org.pvnccdsb.labyrinth.entity.IntelligentEntity;
import org.pvnccdsb.labyrinth.entity.Player;
import org.pvnccdsb.labyrinth.entity.Watcher;
import org.pvnccdsb.labyrinth.utils.Window;
import org.pvnccdsb.labyrinth.utils.general.NumUtils;
import org.pvnccdsb.labyrinth.utils.graphics.Texture;
import org.pvnccdsb.labyrinth.utils.listener.KeyboardListener;
import org.pvnccdsb.labyrinth.utils.manager.EntityManager;
import org.pvnccdsb.labyrinth.world.Location;

public class Labyrinth extends Canvas implements Runnable{

	private static Labyrinth instance;
	private static final long serialVersionUID = -7468017089207931686L;
	public static final String version = "Version ALPHA-0.0.1";
	public static int WIDTH = 720, HEIGHT = WIDTH / 9 * 6;
	
	private Thread thread;
	private boolean running = false;
	private int currentFPS, currentTPS;
	
	private GameState state = GameState.GAME;
	
	private final EntityManager entityManager;
	
	public enum GameState{
		GAME;
	}
	
	public Labyrinth() {
		instance = this;
		
		// Register managers
		this.entityManager = new EntityManager();
		
		// Add listeners
		this.addKeyListener(new KeyboardListener(this));
		
		new Window(WIDTH, HEIGHT, "The Labyrinth", this);
	}
	
	public void init(){
		System.out.println("Loading textures");
		Texture.loadTextures();
		
		System.out.println("Loading entities into the world");
		entityManager.addEntity(new Player(new Location(1, 1), null));
		entityManager.addEntity(new Watcher(new Location(7, 7), null));
		
		// TODO: Load Levels and set the current level using a LevelManager class
	}
	
	public synchronized void start(){
		if (thread == null) thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop(){
		for (Entity entity : entityManager.getEntities()){
			if (entity instanceof IntelligentEntity)
				((IntelligentEntity) entity).getPathfinderGoals().clear();
		}
		entityManager.getEntities().clear();
		
		try { thread.join(); }
		catch (InterruptedException e) { e.printStackTrace(); }
		
		System.exit(0);
	}

	@Override
	public void run() {
		init();
		long lastTime = System.nanoTime();
		double delta = 0, ticksPerSecond = 60.0;
		double ns = 1_000_000_000 / ticksPerSecond;
		long timer = System.currentTimeMillis();
		int frames = 0, ticks = 0;
		while (running){
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				ticks++;
				delta--;
			}
			if (running) render();
			frames++;
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				/* System.out.println("FPS: " + frames) - Print out FPS to console */
				/* System.out.println("TPS: " + ticks) - Print out TPS to console */
				currentFPS = frames; frames = 0;
				currentTPS = ticks; ticks = 0;
			}
		}
		stop();
	}
	
	public void tick(){
		if (state.equals(GameState.GAME)) entityManager.tick();
	}
	
	private static final Font arial16 = new Font("Arial", Font.BOLD, 16);
	public void render(){
		// Create graphics
		BufferStrategy bs = this.getBufferStrategy();
		do{
			Graphics graphics = bs.getDrawGraphics();
			
			graphics.setColor(Color.BLACK);
			graphics.fillRect(0, 0, WIDTH, HEIGHT);
			
			// Render objects
			if (state.equals(GameState.GAME)){
				entityManager.render(graphics);
			}
			
			/* --- TPS, FPS & Version --- */
			if (Debug.DISPLAY_FTPS){
				graphics.setFont(arial16);
				graphics.setColor(currentFPS > 30 ? Color.YELLOW : Color.RED);
				graphics.drawString("FPS: " + currentFPS, 5, 16);
				
				graphics.setColor(Color.YELLOW);
				graphics.drawString("TPS: " + currentTPS, 5, 32);
			}
			
			graphics.setFont(graphics.getFont().deriveFont(Font.ITALIC, 10F));
			graphics.setColor(Color.GRAY);
			graphics.drawString("The Labyrinth - " + version, (WIDTH / 2) - NumUtils.center(graphics, "The Labyrinth - " + version), 10);
			
			// Display & Dispose of graphics
			graphics.dispose();
			bs.show();
			Toolkit.getDefaultToolkit().sync();
		}while(bs.contentsLost());
	}
	
	/** Get an instance of the main class through static-access.
	 * For those tight spots where instance passing isn't quite possible
	 * @return The instance of Labyrinth
	 */
	public static Labyrinth getLabyrinth(){
		return instance;
	}
	
	/** Get the entity manager for the game containing information about registered entities
	 * @return The entity manager
	 */
	public EntityManager getEntityManager(){
		return entityManager;
	}
	
	/** Set the current state of the game
	 * @param state - The state to set
	 */
	public void setState(GameState state){
		this.state = state;
	}
	
	/** Get the state the game is currently in
	 * @return - The state
	 */
	public GameState getState(){
		return state;
	}
	
	/** Set the game to either run or stop running
	 * @param running - The game's running state
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	/** Debug class containing general debug options for the game */
	public static class Debug{
		public static boolean DISPLAY_FTPS = true;
	}
	
	public static void main(String[] args){ new Labyrinth(); }
}